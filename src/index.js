import React from "react";
import ReactDom from "react-dom";
import {Provider} from "react-redux";
import registerServiceWorker from './registerServiceWorker';

import Layout from "./components/Layout";
import store from "./store";

const app = document.getElementById('root');

ReactDom.render(<Provider store={store}>
    <Layout/>
</Provider>, app);
registerServiceWorker();

// import React from 'react';
// import ReactDOM from 'react-dom';
// import './index.css';
// import App from './App';
// import registerServiceWorker from './registerServiceWorker';

// ReactDOM.render(<App />, document.getElementById('root'));
// registerServiceWorker();



//*****************************************************************
// import {createStore, applyMiddleware} from 'redux';
// import axios from "axios";
// import logger from 'redux-logger';
// import thunk from 'redux-thunk';
// import promise from 'redux-promise-middleware';
//
// const initialState = {
//     fetching: false,
//     fetched: false,
//     users: [],
//     error: null
// }
//
// const reducer = (state = initialState, action) => {
//     switch (action.type) {
//         case "FETCH_USER_PENDING": {
//             return {...state, fetching: true};
//         }
//         case "RECIVE_USERS_FULFILLED": {
//             return {...state, fetching: false, fetched: false, users: action.payload};
//         }
//         case "FETCH_USERS_REJECTED": {
//             return {...state, fetching: false, error: action.payload}
//         }
//     }
//     return state;
// }
//
// const middleware = applyMiddleware(promise(), thunk, logger);
// const store = createStore(reducer, middleware);
//
// store.dispatch({
//     type: "FETCH_USERS",
//     payload: axios.get("http://rest.learncode.academy/api/wstern/users")
// });

//
// store.dispatch((dispatch) => {
//     dispatch({type: "FETCH_USER_START"})
//     axios.get("http://rest.learncode.academy/api/wstern/users")
//         .then((response) => {
//             dispatch({type: "RECIVE_USERS", payload: response.data})
//         }).catch((err) => {
//         dispatch({type: "FETCH_USERS_ERROR", payload: err})
//     })
//     // do something async
//     dispatch({type: "BAR"})
// });


//****************************************************
// import {createStore, applyMiddleware} from 'redux';
//
// const reducer = (initialState = 0, action) => {
//     switch (action.type) {
//         case "INC": {
//             return initialState + 1;
//         }
//         case "DEC": {
//             return initialState - 1;
//         }
//         case "E": {
//             throw new Error("asdasds");
//         }
//     }
//     return initialState;
// }
//
// const logger = (store) => (next) => (action) => {
//     console.log('action fired', action);
//     next(action);
// }
//
// const error = (store) => (next) => (action) => {
//     try {
//         next(action)
//     } catch (e) {
//         console.log("Ahhh", e);
//     }
// }
//
// const middleware = applyMiddleware(logger, error);
// const store = createStore(reducer, middleware);
//
// store.subscribe(() => {
//     console.log("store changed", store.getState());
// })
//
// store.dispatch({type: "INC"});
// store.dispatch({type: "INC"});
// store.dispatch({type: "INC"});
// store.dispatch({type: "DEC"});
// store.dispatch({type: "DEC"});
// store.dispatch({type: "DEC"});
// store.dispatch({type: "E"});


//***************************************************************
// const userReducer = function (state = {}, action) {
//     switch (action.type) {
//         case "CHANGE_NAME": {
//             state = {...state, name: action.payload}
//             break;
//         }
//         case "CHANGE_AGE": {
//             state = {...state, age: action.payload};
//             break;
//         }
//     }
//     return state;
// }
//
// const tweetReducer = function (state = {}, action) {
//     return state;
// }
//
// const reducers = combineReducers({
//     user: userReducer,
//     tweet: tweetReducer
// })
//
//
// const store = createStore(reducers);
//
// store.subscribe(() => {
//     console.log("store changed", store.getState());
// });
//
// store.dispatch({type: "CHANGE_NAME", payload: "Will"});
// store.dispatch({type: "CHANGE_AGE", payload: 20});