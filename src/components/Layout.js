import React from "react";
import{connect} from "react-redux";

import {fetchUser, setUserName} from "../actions/userActions";
import {fetchTweets} from "../actions/tweetActions";


class Layout extends React.Component {
    componentDidMount() {
        this.props.dispatch(fetchUser());
    }

    setUserName(e) {
        e.preventDefault();
        this.props.dispatch(setUserName(this.refs.name.value));
    }

    fetchTweets() {
        this.props.dispatch(fetchTweets());
    }

    render() {
        // const {user, tweets} = this.props;
        // if (!tweets.length) {
        //     return ( <div>
        //             <h1>{this.props.users.name}</h1>
        //             <button onClick={this.fetchTweets.bind(this)}>load tweets</button>
        //         </div>
        //     )
        // }
        //
        // const mappedTweets = tweets.map(tweet => <li key={tweet.id}>{tweet.text}</li>);
        // return (
        //     <div>
        //         <h1>{this.props.users.name}</h1>
        //         <ul>
        //             {mappedTweets}
        //         </ul>
        //     </div>
        // )

        return (
            <div>
                <h1>{this.props.users.name}</h1>
                <form onSubmit={this.setUserName.bind(this)}>
                    <input type="text" ref="name"/>
                    <input type="submit" />
                </form>


            </div>
        );
    }
}

export default connect((store) => {
    return {
        users: store.users.user,
        tweets: store.tweets.tweets,
        userFetched: store.users.fetched,
    }
})(Layout);